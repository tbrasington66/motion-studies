/** @jsx jsx */
import { Box, Flex, jsx } from 'theme-ui';
import { useState } from 'react';
import { useSpring, animated } from 'react-spring';
import { Label1, Label2 } from '@66north/core-ui';
import { useLockBodyScroll } from './hooks';

const data = [
	{
		label: 'Men',
		children: [
			{
				label: 'Collection',
				children: [
					{ label: 'Parkas, Jackets and Coats' },
					{ label: 'Tops' },
					{ label: 'Bottoms' },
					{ label: 'Accessories' }
				]
			},
			{
				label: 'Ideas and Guides',
				children: [
					{ label: 'Coat Finder' },
					{ label: 'How to layer' },
					{ label: 'Exploring Iceland ' },
					{ label: 'Sports and activities' },
					{ label: 'Compare products' }
				]
			},
			{
				label: 'Sagas:Our Icons',
				children: [
					{ label: 'Drangajökull' },
					{ label: 'Jökla' },
					{ label: 'Tindur Down' },
					{ label: 'Dyngja' },
					{ label: 'Snæfell' }
				]
			}
		]
	},
	{
		label: 'Women',
		children: []
	},
	{
		label: 'Children',
		children: []
	},
	{
		label: 'Read',
		children: []
	},
	{
		label: 'Circular',
		children: []
	},
	{
		label: 'Service',
		children: []
	}
];

const Menu = () => {
	// lock on mount
	useLockBodyScroll();

	// state
	const [ { index, level2 }, set ] = useState({ index: 0 });

	const LinksPanel = ({ data }) => {
		return (
			<Box sx={{ position: 'absolute', left: 56, flexDirection: 'column' }}>
				{data.map((item, index) => {
					return (
						<Box key={index} sx={{ height: 48 }}>
							<Label1
								as="a"
								sx={{ color: 'white.0' }}
								onClick={() => set({ index: 1, level2: item.children })}
							>
								{item.label}
							</Label1>
						</Box>
					);
				})}
			</Box>
		);
	};

	const Level2Panel = ({ data }) => {
		return data ? (
			<Box sx={{ position: 'absolute', left: 56 }}>
				<Box
					as="button"
					sx={{
						color: 'white.0',
						variant: 'text.label2',
						p: 0,
						mb: 24,
						webkitAppearance: 'none',
						border: 'none',
						bg: 'transparent'
					}}
					onClick={() => set({ index: 0, level2: level2 })}
				>
					Back
				</Box>

				<Flex sx={{ flexDirection: 'column', width: '100%' }}>
					{data &&
						data.map((item, index) => {
							return (
								<Box key={index} sx={{ mb: 24 }}>
									<Label2 sx={{ color: 'grey.2', mb: 8 }}>{item.label}</Label2>
									{item.children &&
										item.children.map((child, index2) => {
											return (
												<Box key={index + '+' + index2} sx={{ height: 48 }}>
													<Label1 as="a" sx={{ color: 'white.0' }}>
														{child.label}
													</Label1>
												</Box>
											);
										})}
								</Box>
							);
						})}
				</Flex>
			</Box>
		) : (
			<Box sx={{ position: 'absolute', left: 56 }} />
		);
	};
	
	// !important
	// this needs to be rewritten using useTransition as we need the enter and leave animations for when the menu is closed
	const level1Animation = useSpring({
		from: {
			opacity: index === 0 ? 0 : 1,
			transform: index === 0 ? 'translate3d(-10%,0,0)' : 'translate3d(0%,0,0)'
		},
		to: { opacity: index === 0 ? 1 : 0, transform: index === 0 ? 'translate3d(0%,0,0)' : 'translate3d(-20%,0,0)' },
		reset: true
	});
	const level2Animation = useSpring({
		from: {
			opacity: index === 1 ? 0 : 1,
			transform: index === 1 ? 'translate3d(20%,0,0)' : 'translate3d(0%,0,0)'
		},
		to: {
			opacity: index === 1 ? 1 : 0,
			transform: index === 1 ? 'translate3d(0%,0,0)' : 'translate3d(30%,0,0)'
		},
		reset: true
	});

	return (
		<Box sx={{ mt: '8rem', overflowY: 'scroll', width: '100vw' }}>
			<animated.div style={{ opacity: level1Animation.opacity, transform: level1Animation.transform }}>
				<LinksPanel data={data} />
			</animated.div>

			<animated.div style={{ opacity: level2Animation.opacity, transform: level2Animation.transform }}>
				<Level2Panel data={level2} />
			</animated.div>
		</Box>
	);
};

export default Menu;
