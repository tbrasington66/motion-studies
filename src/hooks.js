import {useLayoutEffect, useRef, useEffect,useState} from 'react' //useState,
import ResizeObserver from 'resize-observer-polyfill' 

export function useLockBodyScroll() {
	useLayoutEffect(() => {
	 // Get original body overflow
	 const originalStyle = window.getComputedStyle(document.body).overflow;  
	 // Prevent scrolling on mount
	 document.body.style.overflow = 'hidden';
	 // Re-enable scrolling when component unmounts
	 return () => document.body.style.overflow = originalStyle;
	 }, []); // Empty array ensures effect is only run on mount and unmount
  }


export function usePrevious(value) {
  const ref = useRef()
  useEffect(() => void (ref.current = value), [value])
  return ref.current
}

export function useMeasure() {
  const ref = useRef()
  const [bounds, set] = useState({ left: 0, top: 0, width: 0, height: 0 })
  const [resize] = useState(() => new ResizeObserver(([entry]) => set(entry.contentRect)))
  useEffect(() => {
    if (ref.current) resize.observe(ref.current)
    return () => resize.disconnect()
  }, [resize])
  return [{ ref }, bounds]
}
