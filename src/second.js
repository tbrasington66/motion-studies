/** @jsx jsx */
import { defaultTheme } from '@66north/core-ui';
import '@66north/shared-css/index.css';
import { Box, Flex, jsx, Styled, ThemeProvider } from 'theme-ui';
import { useState } from 'react';
import { useSpring, animated } from 'react-spring';
import { useMeasure, usePrevious } from './hooks';
import Menu from './Menu';
import PageContent from './PageContent';

function App() {
	const [ isOpen, setOpen ] = useState(false);

	const previous = usePrevious(isOpen);
	const [ bind, { height: viewHeight } ] = useMeasure();

	const { transform, height } = useSpring({
		from: {
			height: viewHeight
		},
		to: {
			transform: `scale(${isOpen ? 0.5 : 1})`,
			height: isOpen ? window.innerHeight : viewHeight
		},
		onRest: () => {}
	});

	return (
		<ThemeProvider theme={defaultTheme}>
			<Styled.root>
				<Flex
					sx={{
						width: '100vw',
						flexDirection: 'column',
						overflow: 'hidden'
					}}
				>
					<Box
						sx={{
							zIndex: 3,
							position: 'sticky',
							top: 0,
							left: 0,
							bg: 'black.0',
							width: '100%',
							height: '64px',
							variant: 'text.sixth',
							color: 'white'
						}}
					
					>
						<Box 	onClick={() => setOpen(!isOpen)}>Menu</Box>
            <Box sx={{float:'right'}}>Logo</Box>
					</Box>

					<animated.div
						style={{
							willChange: 'transform',
							transform,
							height: isOpen && previous === isOpen ? window.innerHeight : height,
							width: '100%',
							zIndex: 2,
							transformOrigin: '150%',
							minHeight: '100%',
							overflow: isOpen ? 'hidden' : 'visible'
						}}
					>
						<Flex
							sx={{
								left: 0,
								bg: 'grey.4',
								width: '100%',
								minHeight: '100%',
								flexDirection: 'column'
							}}
							{...bind}
						>
							<PageContent sx={{ maxWidth: '60ch', p: 5 }} />
						</Flex>
					</animated.div>
					<Flex
						sx={{
							position: 'absolute',
							zIndex: 1,
							top: 0,
							left: 0,
							bg: 'blue.1',
							width: '100%',
							height: '100%',
							p: 5
						}}
					>
						{isOpen && <Menu />}
					</Flex>
				</Flex>
			</Styled.root>
		</ThemeProvider>
	);
}

export default App;
