/** @jsx jsx */
import { defaultTheme } from '@66north/core-ui';
import '@66north/shared-css/index.css';
import { Box, Flex, jsx, Styled, ThemeProvider } from 'theme-ui';
import { useState, useRef } from 'react';
import { useSpring, animated } from 'react-spring';
import Menu from './Menu';
import PageContent from './PageContent';
import ColorLogo from './Logo.js'

function App() {
	const AnimatedRef = useRef(null);
	const PageRef = useRef(null);

	const [ { isOpen, previousScroll }, setOpen ] = useState({ isOpen: false });

	const { transform, height } = useSpring({
		from: {
			height: window.innerHeight - 80
		},
		to: {
			transform: `scale(${isOpen ? 0.5 : 1})`,
			height: window.innerHeight - 80
		},
		config: {
			mass: 1,
			tension: 200,
      friction: 26,
      clamp:true,
      easing: 'ease-in'
		},
		onStart: () => {
			if (isOpen) {
				PageRef.current.style.marginTop = previousScroll * -1 + 'px';
				AnimatedRef.current.style.overflow = 'hidden';
				AnimatedRef.current.style.height = window.innerHeight - 80;
			}
		},
		onRest: () => {
			console.log(previousScroll);
			if (!isOpen) {
				PageRef.current.style.marginTop = '0';
				AnimatedRef.current.style.overflow = 'visible';
				AnimatedRef.current.style.height = 'auto';
				window.scroll(0, previousScroll);
			}
		}
	});

	return (
		<ThemeProvider theme={defaultTheme}>
			<Styled.root>
				<Flex
					sx={{
						width: '100vw',
						flexDirection: 'column',
						overflow: 'hidden'
					}}
				>
					<Box
						sx={{
							zIndex: 3,
							position: 'fixed',
							top: 0,
							left: 0,
							bg: isOpen ? 'blue.1' : 'white.0',
							width: '100%',
              height: '80px',
              display : 'flex'
						}}
					>
						<Box
							as="button"
							onClick={() => {
								setOpen({ isOpen: !isOpen, previousScroll: !isOpen ? window.scrollY : previousScroll });
							}}
							sx={{
								color: isOpen ? 'white.0' : 'black.1',
                variant: 'text.heading6',
                px: 24,
                webkitAppearance: 'none',
                border : 'none',
                bg : 'transparent'
                
							}}
						>
							{isOpen ? 'Close' : 'Menu'}
						</Box>
            <Flex sx={{flex:1, pr : 16, alignItems:'center', justifyContent : 'flex-end'}}><ColorLogo/></Flex>
					</Box>

					<animated.div
						ref={AnimatedRef}
						style={{
							willChange: 'transform height',
							width: '100%',
							zIndex: 2,
							transformOrigin: '150%',
							minHeight: '100%',
							paddingTop: '80px',
							transform,
							height
						}}
					>
						<Flex
							ref={PageRef}
							sx={{
								bg: 'grey.4',
								width: '100%',
								minHeight: '100%',
								flexDirection: 'column'
							}}
						>
							<PageContent sx={{ maxWidth: '60ch', p: 5 }} />
						</Flex>
					</animated.div>
					<Flex
						sx={{
							position: 'absolute',
							zIndex: 1,
							top: 0,
							left: 0,
							bg: 'blue.1',
							width: '100%',
							height: '100%',
							p: 5
						}}
					>
						{isOpen && <Menu />}
					</Flex>
				</Flex>
			</Styled.root>
		</ThemeProvider>
	);
}

export default App;
